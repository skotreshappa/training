#include <iostream>

using namespace std;

class MusicInstrument {
    public:
    virtual void play() { cout << "Play Instrument " << endl; }
    virtual void reset() { cout << "Restting instrument .." << endl; }
    virtual ~MusicInstrument(){};
};

class Machine {
    public:
    virtual void start() { cout << "Starting Machine " << endl; }
    virtual void reset() { cout << "Resetting machine .." << endl; }
    virtual ~Machine(){};
};

class Synthesizer: public Machine, public MusicInstrument {
    public:
    virtual ~Synthesizer(){};
};

int main() {
    Synthesizer *pSynth = new Synthesizer();

    pSynth->play();
    pSynth->start();
    pSynth->MusicInstrument::reset();

    delete pSynth;
    return 0;
}

