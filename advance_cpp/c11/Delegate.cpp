#include <iostream>
using namespace std;

class Parent {
    int dogs{5};
    string text{"hello"};

    public:
    Parent() : Parent("hell") {
        cout << "No Parameter parent constructor" << endl;
    }

    Parent(string text) {
        cout << "string parent constructor" << endl;
    }
};

class Child:public Parent {
    public:
        Child() = default; 
};

int main() {
    Parent parent("Hell");
    Child child;

    return 0;
}
