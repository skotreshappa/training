#include <iostream>
#include <vector>
using namespace std;

class Test {
    private:
        static const int SIZE =  100;
        int *_pBuffer{nullptr};
    public:
        Test() {
            _pBuffer = new int[SIZE] {};
        }

        Test(int i) {
            _pBuffer = new int[SIZE] {};

            for(int i=0;i<SIZE;i++) {
                _pBuffer[i] = 7 * i;
            }
        }

        Test(const Test &other) {
            _pBuffer = new int[SIZE] {};

            memcpy(_pBuffer, other._pBuffer, SIZE * sizeof(int));
        }

        Test &operator()(const Test &other) {
            _pBuffer = new int[SIZE] {};

            memcpy(_pBuffer, other._pBuffer, SIZE * sizeof(int));
            return *this;
        }

        Test(Test &&other) {
            cout << "Move constructor" << endl;
            _pBuffer = other._pBuffer;
            other._pBuffer = nullptr;
        }

        Test &operator=(Test &&other) {
            cout << "Move assignment" << endl;
            delete [] _pBuffer;
            _pBuffer = other._pBuffer;
            other._pBuffer = nullptr;

            return *this;
        }


        ~Test() {
            delete [] _pBuffer;
        }

        friend ostream &operator<<(ostream &out, const Test &test);
};

ostream &operator<<(ostream &out, const Test &test) {
    out << "Hello from test";
    return out;
}

Test getTest() {
    return Test();
}

void check(const Test &value) {
    cout << "lvalue function" << endl;
}

void check(const Test &&value) {
    cout << "rvalue function" << endl;
}

int main() {
    Test test1 = getTest();

    cout << test1 << endl;

    vector<Test> vec;
    vec.push_back(Test());

    int value1 = 7;

    int *pValue1 = &value1;

    Test *pTest1 = &test1;

    int *pValue3 = &++value1;
    cout << *pValue3 << endl;

    //int *pValue4 = &value1++;

    Test &rTest1 = test1;
    Test &rTest2 = getTest();

    const Test &rTest3 = getTest();

    Test test2(Test(1));

    Test &&rtest2 = getTest(); 
    check(test1);
    check(getTest());
    check(Test());

    Test test4;
    test4 = getTest();

    return 0;
}
