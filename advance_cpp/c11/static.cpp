#include <iostream>
using namespace std;

class Parent {
    public:
        virtual void speak() {
            cout << "Parent !" << endl;
        }
};

class Brother: public Parent {
};

class Sister: public Parent {
};

int main() {
    Parent parent;
    Brother brother;

    float value = 3.23;

    cout << static_cast<int>(value) << endl;


    Parent *pp = &brother;
    Brother *pb = static_cast<Brother *>(&parent);
    cout << pb << endl;


    Brother *pbb = static_cast<Brother *>(pp);

    cout << pbb << endl;

    Parent &&p = static_cast<Parent &&>(parent); 
    p.speak();

    Parent *ppp = &parent;
    Brother *pppb = dynamic_cast<Brother *>(ppp);

    if (pppb == nullptr) {
        cout << "Invalid cast" << endl;
    } else {
        cout << pppb << endl;
    }

    Sister sister;
    
    ppp = &brother;
    Sister *ppps = reinterpret_cast<Sister *>(ppp);

    if (ppps == nullptr) {
        cout << "Invalid cast" << endl;
    } else {
        cout << ppps << endl;
    }


    return 0;
}
