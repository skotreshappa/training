#ifndef FRACTALCREATOR_H
#define FRACTALCREATOR_H

#include <string>
#include <vector>
#include "Zoom.h"
#include "Bitmap.h"
#include "ZoomList.h"
#include "RGB.h"

using namespace std;

namespace caveofprogramming {

class FractalCreator {
    private:
    int m_width{0};
    int m_height{0};
    int m_total{0};
    unique_ptr<int[]> m_histogram;
    unique_ptr<int[]> m_fractal;
    Bitmap m_bitmap;
    ZoomList m_zoomList;

    vector<int> m_ranges;
    vector<RGB> m_colors;
    vector<int> m_rangeTotals;

    bool m_bGotFirstRange{false};

    void calculateIteration();
    void calculateTotalIterations();
    void calculateRangeTotals();
    void drawFractal();
    void writeBitmap(string name);

    public:
        FractalCreator(int width, int height);
        virtual ~FractalCreator();
        void addZoom(const Zoom& zoom);
        void addRange(double rangeEnd, const RGB& rgb);
        void run(string name);
    int getRange(int iterations) const;
};

}
#endif /* FRACTALCREATOR_H */
