#include <iostream>
#include <memory>

using namespace std;

class Test {
    public:
        Test() {
            cout << "created" << endl;
        }

        void greet() {
            cout << "Hello" << endl;
        }

        ~Test() {
            cout << "destroyed" << endl;
        }
};

class Temp {
    private:
        unique_ptr<Test[]> pTest;
    public:
        Temp(): pTest(new Test[2]) {
        }
};


int main() {
    unique_ptr<int> pTest(new int);

    *pTest = 7;

    cout << *pTest << endl;
    Temp temp;
    //{
        //unique_ptr<Test[]> ppTest(new Test[2]);
        //ppTest[1].greet();
    //}

    cout << "Finished" << endl;
    return 0;
}
