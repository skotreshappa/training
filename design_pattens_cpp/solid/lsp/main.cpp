#include <iostream>
#include <vector>

using namespace std;

class Rectangle
{
    protected:
        int m_width, m_height;

    public:
        Rectangle(int width, int height): m_width(width), m_height(height) {}

        int getWidth() const {
            return m_width;
        }

        virtual void setWidth(int width) {
            m_width = width;
        }

        int getHeight() const {
            return m_height;
        }

        virtual void setHeight(int height) {
            m_height = height;
        }

        int area() const { return m_width * m_height;}
};

class Square: public Rectangle
{
    public:
        Square(int size): Rectangle(size, size) {}

        void setWidth(int width) override {
            this->m_width = this->m_height = width;
        }

        void setHeight(int height) override {
            this->m_height = this->m_width = height;
        }
        
};

void process(Rectangle& r)
{
    int w = r.getWidth();
    r.setHeight(10);

    cout << "expected area = " << (w*10)
        << ", got " << r.area() << endl;
}

struct RectangleFactory
{
    static Rectangle create_rectangle(int w, int h);
    static Rectangle create_square(int size);
};


int main()
{
    Rectangle r{3,4};
    process(r);

    Square s{5};
    process(s);

    return 0;
}

