
class ProductRepository {
    public:
        virtual vector<string> getAllProductNames() = 0;
};

class SqlProductRepository: public ProductCatalog {
    public:
        vector<string> getAllProductNames() { }
};

class ProductFactory {
    public:
        static ProductFactory create() {
            return SqlProductRepository();
        }
};

class ProductCatalog {
    public:
        void listAllProducts() {
            ProductRepository productRepo = ProductFactory.create();
            productRepo.getAllProductNames();
        }
};
