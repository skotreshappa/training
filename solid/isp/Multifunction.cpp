
class IPrint {
    virtual void print() = 0;
};

class IScan {
    virtual void scan() = 0;
};

class IFax {
    virtual void fax() = 0;
};

class XeroxWorkCentre: public IPrint, IScan, IFax {
    void print() {}
    void scan() {}
    void fax() {}
};

class HpStation: public IPrint, IScan {
    void print() {}
    void scan() {}
};

class Cannon: public IPrint {
    void print() {}
};


int main() {
    return 0;
}
