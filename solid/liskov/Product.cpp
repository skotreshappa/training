#include <iostream>
#include <vector>

using namespace std;

class Product {
    protected:
        double discount{20};

    public:
        virtual double getDiscount() {
            return discount;
        }
};

class InHouseProduct: public Product {
    public:
        void applyExtraDiscount() {
            discount = discount * 1.5;
        }

        double getDiscount() {
            cout << "I am " << endl;
            applyExtraDiscount();
            return discount;
        }
};

int main() {
    Product p1 = Product();
    Product p2 = Product();
    InHouseProduct p3 = InHouseProduct();
    Product *p4 = &p3;
    Product *p5 = &p1;
    Product *p6 = &p2;

    vector<Product*> productList;

    productList.push_back(p4);
    productList.push_back(p5);
    productList.push_back(p6);

    for(auto product : productList) {
        cout << product->getDiscount() << endl;
    }
}

