
class CustomerProfile {
    public:
        virtual bool isLoyalCustomer() = 0;
};

class HealthInsuranceCustomerProfile: public CustomerProfile {
    public:
        bool isLoyalCustomer() {
            return random.bool;
        }
};

class VehicleInsuranceCustomerProfile: public CustomerProfile {
    public:
        bool isLoyalCustomer() {
            return random.bool;
        }
};

class HomeInsuranceCustomerProfile: public CustomerProfile {
    public:
        bool isLoyalCustomer() {
            return random.bool;
        }
};

class InsurancePremiumDiscountCalculator {
    public:
        int calculatePremiumDiscountPercent(CustomerProfile &customer) {
            if (Customer.isLoyalCustomer()) {
                return 20;
            }

            return 0;
        }
};

